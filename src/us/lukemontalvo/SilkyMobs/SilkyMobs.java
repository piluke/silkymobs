package us.lukemontalvo.SilkyMobs;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class SilkyMobs extends JavaPlugin {
	public static String version = "SilkyMobs v1.0";
	private static Logger logger = Logger.getLogger("minecraft");

	public void onEnable() {
		getServer().getPluginManager().registerEvents(new SilkyMobsBlockListener(), this);

		log("Enabled!");
	}
	public void onDisable() {
		log("Disabled!");
	}

	public static void log(String s, boolean is_debug_msg) {
		boolean is_debugging = false;
		//boolean is_debugging = true;
		if ((is_debugging)||(!is_debug_msg)) {
			SilkyMobs.logger.log(Level.INFO, String.format("[SilkyMobs] %s", s));
		}
	}
	public static void log(String s) {
		log(s, false);
	}
	public static void senderLog(CommandSender sender, Level level, String s) {
        if (level == Level.INFO) {
            s = String.format("%s%s", ChatColor.GREEN, s);
        } else if (level == Level.WARNING) {
            s = String.format("%s%s", ChatColor.RED, s);
        }
        sender.sendMessage(s);
    }
}
